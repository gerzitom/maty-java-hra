package logika;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
class PrikazJdi implements IPrikaz {
    private static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Hra hra;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param plan herní plán, ve kterém se bude ve hře "chodit" 
    */    
    public PrikazJdi(HerniPlan plan, Hra hra) {
        this.plan = plan;
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru.
     *  Pokud zadaný sousední prostor (východ) není, vypíše se chybové hlášení.
     *  Kontroluje konec hry: hra skončí v případě, že je hráč dehydratovaný nebo když dojde do pokoje a vyhraju hru
     *
     *  @param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *  @return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = plan.getAktualniProstor().vratSousedniProstor(smer);

        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        } else {
            if (sousedniProstor.jeZamceno()) {
                return "dveře do místnosti "+sousedniProstor.getNazev()
                 +" jsou zamčené";
            } else if (!sousedniProstor.jeViditelny()){
                return "Tam se odsud jít nedá!";
            }


            this.plan.snizKrokyPredDehydrataci();
            if(this.plan.getKrokyPredDehydrataci() <= 2){
                System.out.println("Už začínáš být opravdu žíznivý, proto bys měl použít sklenici s vodou a napít se, jinak brzy zemřeš!");
            }
            if (this.plan.getKrokyPredDehydrataci() == 0){
                System.out.println("Právě si umřel dehydratací, měl ses napít sklenice vody!");
                this.hra.setKonecHry(true);
            }

            plan.setAktualniProstor(sousedniProstor);
            System.out.println(sousedniProstor.dlouhyPopis());
            if(sousedniProstor.getNazev().equals("pokoj")){
                this.hra.setKonecHry(true);
            }
            return "";
        }
    }
    
    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
