/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import java.util.Set;


/*******************************************************************************
 * Instance třídy PrikazMluv představují ...
 *
 * @author    Matyáš Frank
 * @version   0.00.000
 */
public class PrikazOtevrit implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================

    private static final String NAZEV = "otevrit";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public PrikazOtevrit(HerniPlan plan){
        this.plan = plan;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================

    public String provedPrikaz(String... parametry){
       if (parametry.length == 0) {
           // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Co chceš otevřít?";
       }
       if (parametry.length == 1){
           String predmet = parametry[0];
           for(Vec vec : this.plan.getAktualniProstor().getPritomneVeci()){
               if (predmet.equals(vec.getJmeno())){
                   vec.provedInterakci(plan);
               }
           }
           return null;
       }
       else {
            return "Zadal si moc  příkazů, nevím, co tím myslíš!";
       }
    }
    //pracuje se seznamem prikazu IPrikaz
    public String getNazev(){
        return NAZEV;
    }

    //== Soukromé metody (instancí i třídy) ========================================

}
