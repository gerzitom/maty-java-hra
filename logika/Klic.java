package logika;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Klic extends Vec {

    private Set<Vec> inventar;
    private String odemyka;

    public Klic(String jmeno, String odemyka) {
        super(jmeno);
        this.odemyka = odemyka;
        this.inventar = new HashSet<Vec>();
    }

    @Override
    public String getJmeno() {
        return super.getJmeno();
    }

    @Override
    public void provedInterakci(HerniPlan plan) {
        if(plan.getAktualniProstor().vratSousedniProstor(this.odemyka) != null){
            plan.getProstor(this.odemyka).odemkni();
            plan.getBatoh().oddelejVecZBatohu(this.getJmeno());
        } else {
            System.out.println("Tento předmět nemůže být použit tímto způsobem!");
        }
    }
}


