/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy PrikazDej představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class PrikazDej implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    
    private static final String NAZEV = "dej";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public PrikazDej(HerniPlan plan)
    {
        this.plan = plan;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================
    
    public String provedPrikaz(String... parametry){
       if (parametry.length == 0) {
           // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Musíš mi něco říct";
       }
       //Rozdává tatranku, ověřuje, za ji má v batohu, ověřuje platnost postavy a připadne ji po odevzdani z batohu odstrani
       if (parametry.length == 2){ //parametry pro příkaz dej - komu, co
           String nazevVeci = parametry[0];
           String nazevOsoby = parametry[1];
           Vec tatranka = this.plan.getBatoh().najdiVBatohu("tatranka"); //vyhleda tatranku v batohu
           if(tatranka != null){
               // pokud je tatranka v batohu
               for(Postava postava : plan.getAktualniProstor().getPritomnePostavy()){
                   if(postava.getJmeno().equals(nazevOsoby)){
                       if(postava.overJestliChceTuhleVec("tatranka")){
                           this.plan.getBatoh().oddelejVecZBatohu("tatranka");
                           this.plan.getProstor("strom").zviditelnit();
                           System.out.println(postava.prijmoutVec());
                           this.plan.getBatoh().vlozVec(new Klic("lano", "strom"));
                           return "";
                       }
                   }
               }
               return "Postava, které chceš dát tuto věc neexistuje!";
           }
           return "Tuto věc nemáš v batohu!";
       }
       else {
            return "Řekl jsi " + parametry[0];
       }
    }

   public String getNazev(){
       return NAZEV;
   }

    //== Soukromé metody (instancí i třídy) ========================================

}
