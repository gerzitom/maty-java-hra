package logika;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Truhla extends Vec {

    private Set<Vec> inventar;

    public Truhla() {
        super("truhlice");
        this.inventar = new HashSet<Vec>();
    }

    public void pridejVec(Vec vec){
        this.inventar.add(vec);
    }

    @Override
    public String getJmeno() {
        return super.getJmeno();
    }

    @Override
    public void provedInterakci(HerniPlan plan) {
        System.out.println("V truhle jsou tyto veci: ");
        this.vypisVeci();
        System.out.println("Zadejte kterou věc chcete vzít:");
        Scanner scanner = new Scanner(System.in);
        String nazevVeci = scanner.nextLine();
        Vec vecZTruhly = this.vratVec(nazevVeci);
        if(vecZTruhly != null){
            plan.getBatoh().vlozVec(vecZTruhly);
        }
    }

    public void vypisVeci(){
        for (Vec vec : this.inventar){
            System.out.print(vec.getJmeno() + " ");
        }
        System.out.println();
    }

    public Vec vratVec(String nazevVeci){
        for (Vec vec : this.inventar){
            if(vec.getJmeno().equals(nazevVeci)){
                return vec;
            }
        }
        return null;
    }


}


