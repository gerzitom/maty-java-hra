/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy PrikazPouzij představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class PrikazPouzij implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================

    private static final String NAZEV = "pouzij";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public PrikazPouzij(HerniPlan plan)
    {
        this.plan = plan;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================

    public String provedPrikaz(String... parametry){
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
             return "Musíš něco použít!";
        }
        if (parametry.length == 1){
            String nazevPredmetu = parametry[0];
            Vec vec = this.plan.getBatoh().najdiVBatohu(nazevPredmetu);
            if(vec != null){
                vec.provedInterakci(this.plan);
                return "";

//                if(vec.getJmeno().equals("lano")){
//                    if(this.plan.getAktualniProstor().getNazev().equals("les")){
//                        this.plan.getProstor("strom").odemkni();
//                        return "";
//                    } else {
//                        return "Pro použití lana musíš být v lese!";
//                    }
//                } else if(vec.getJmeno().equals("klic")){
//                    if(this.plan.getAktualniProstor().getNazev().equals("les")){
//                        this.plan.getProstor("chaloupka").odemkni();
//                        return "";
//                    } else {
//                        return "Pro použití klíče musíš být v lese!";
//                    }
//                }
            } else {
                return "Tuto věc nemáš v batohu!";
            }
        }
        return "Musíš použít jen jeden příkaz";
    }

    public String getNazev(){
        return NAZEV;
    }
    

    //== Soukromé metody (instancí i třídy) ========================================

}
