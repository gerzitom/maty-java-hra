package logika;

import java.util.Set;
import java.util.HashSet;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {
    
    private Prostor aktualniProstor;
    private Set<Postava> seznamPostav;     //pole seznamu postav, pro prikaz mluv
    private Batoh batoh;
    private Set<Prostor> vsechnyProstory;
    private int krokyPredDehydrataci;
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        seznamPostav = new HashSet<>();
        vsechnyProstory = new HashSet<>();
        this.batoh = new Batoh();
        this.krokyPredDehydrataci = 3;
        zalozProstoryHry();
    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor chaloupka = new Prostor("chaloupka", "chaloupka, zde najdes pokoj pro prenocovani ");
        Prostor les = new Prostor("les","les s jahodami, malinami a pramenem vody");
        Prostor hlubokyLes = new Prostor("hluboký_les","hluboký les, jsi ztracen");
        Prostor pokojVChaloupce = new Prostor("pokoj", "pokoj v chaloupce, zde jsi došel do cíle, můžeš zde přespat.");
        Prostor strom = new Prostor("strom", "Jsi na stromě!");

        vsechnyProstory.add(chaloupka);
        vsechnyProstory.add(les);
        vsechnyProstory.add(hlubokyLes);
        vsechnyProstory.add(pokojVChaloupce);
        vsechnyProstory.add(strom);
        
        strom.zneviditelnit();
        strom.zamkni();
        chaloupka.zamkni();
        pokojVChaloupce.zamkni();

        Truhla truhla = new Truhla();
        Klic klic = new Klic("klic", "chaloupka");
        truhla.pridejVec(klic);
        strom.pridejVec(truhla);

        Klic pacidlo = new Klic("pacidlo", "pokoj");
        les.pridejVec(pacidlo);

        Vec excalibur = new Vec("excalibur");
        excalibur.pripevnitKZemi();
        les.pridejVec(excalibur);

        // přiřazují se průchody mezi prostory (sousedící prostory)
        hlubokyLes.setVychod(les);
        les.setVychod(hlubokyLes);
        les.setVychod(chaloupka);
        chaloupka.setVychod(les);
        strom.setVychod(les);
        les.setVychod(strom);
        chaloupka.setVychod(pokojVChaloupce);
        pokojVChaloupce.setVychod(chaloupka);

        Postava Krakonos = new Postava("Krakonoš", "Mám hlad, pokud mi dáš něco k jídlu, řeknu ti tajemství.", "tatranka");

        this.seznamPostav.add(Krakonos);

        les.pridejPostavu(Krakonos);
        
        batoh.vlozVec(new Vec("tatranka"));
        batoh.vlozVec(new Lahev(10));

        this.aktualniProstor = hlubokyLes;  // hra začíná v hlubokem lese
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
   public Batoh getBatoh(){
       return this.batoh;
   }

   public Prostor getProstor(String nazevProstoru){
       for(Prostor prostor : this.vsechnyProstory){
           if(prostor.getNazev().equals(nazevProstoru)){
               return prostor;
           }
       }
       return null;
   }
  
   

    public Set<Postava> getSeznamPostav(){
        return this.seznamPostav;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        this.aktualniProstor = prostor;
    }

    public int getKrokyPredDehydrataci() {
        return krokyPredDehydrataci;
    }

    public void snizKrokyPredDehydrataci(){
        this.krokyPredDehydrataci--;
    }

    public void zvysKrokyPredDehydrataci(int pocet){
        this.krokyPredDehydrataci += pocet;
    }
}
