/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import java.util.Set;
import java.util.HashSet;


/*******************************************************************************
 * Instance třídy Batoh představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class Batoh
{
    //== Datové atributy (statické i instancí)======================================
    
    private Set<Vec> inventar;
    private int velikostBatohu;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public Batoh() {
        this.velikostBatohu = 2;
        this.inventar = new HashSet<Vec>();
    }

    //== Nesoukromé metody (instancí i třídy) ======================================
    //Vloží věc do batohu
    public void vlozVec(Vec vec){
        if(inventar.size() < this.velikostBatohu){
            this.inventar.add(vec);
            System.out.println("Do batohu byla vlozena vec " + vec.getJmeno());
        } else {
            System.out.println("Do batohu se už nic nevleze!");
        }
    }

    //Najde v batohu
    public Vec najdiVBatohu(String nazevVeci){
        for(Vec vec : this.inventar){
            if(vec.getJmeno().equals(nazevVeci)){
                return vec;
            }
        }
        return null;
    }

    public void oddelejVecZBatohu(String nazevVeci){
        this.inventar.remove(this.najdiVBatohu(nazevVeci));
    }

    public String vratCelyBatoh(){
        String ret = "";
        for(Vec vec : this.inventar){
            ret += vec.getJmeno() + " ";
        }
        return ret;
    }

    //== Soukromé metody (instancí i třídy) ========================================

}
