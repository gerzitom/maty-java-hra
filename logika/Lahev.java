package logika;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Lahev extends Vec {

    private int kapacita;

    public Lahev(int kapacita) {
        super("lahev");
        this.kapacita = kapacita;
    }

    @Override
    public String getJmeno() {
        return super.getJmeno();
    }

    @Override
    public void provedInterakci(HerniPlan plan) {
        plan.zvysKrokyPredDehydrataci(this.kapacita);
        plan.getBatoh().oddelejVecZBatohu(this.getJmeno());
        System.out.println("Ahhh, to jsem se krásně napil, teď vydržím celou noc.");
    }

}


