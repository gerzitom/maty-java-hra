/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy Vec představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class Vec
{
    private String jmeno;
    private Boolean jdeVzit;
    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public Vec(String jmeno) {
        this.jmeno = jmeno;
        this.jdeVzit = true;
    }

    public String getJmeno() {
        return this.jmeno;
    }

    public Boolean jdeVzit() {
        return jdeVzit;
    }

    public void pripevnitKZemi(){
        this.jdeVzit = false;
    }



    public void provedInterakci(HerniPlan plan){
        System.out.println("S touto věcí se nedá manipulovat!");
    }

    //== Nesoukromé metody (instancí i třídy) ======================================


    //== Soukromé metody (instancí i třídy) ========================================

}
