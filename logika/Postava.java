/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;



/*******************************************************************************
 * Instance třídy Postava představují ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class Postava
{
    //== Datové atributy (statické i instancí)======================================
    private String jmeno;
    private String jehoRec;
    private String nazevVeciCoChce;
    private Boolean primaVeci;
    private Boolean jeSpokojena;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public Postava(String jmeno, String rec, String nazevVeciCoChce) {
        this.jmeno = jmeno;
        this.jehoRec = rec;
        this.primaVeci = false;
        this.nazevVeciCoChce = nazevVeciCoChce;
        this.jeSpokojena = false;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================
 
    public String getRec(){
        return this.jehoRec;
    }

    public String getJmeno() {
        return this.jmeno;
    }
    
    public void zacinaBratVeci(){
        this.primaVeci = true;
    }
    
    public String prijmoutVec(){
        this.jeSpokojena = true;
        return "Děkuji, to jsem si pochutnal, za to ti řeknu, že v lese je jeden strom, kde najdeš truhlu s klíčem od chaloupky.";
    }

    public Boolean jeSpokojena() {
        return jeSpokojena;
    }

    /*
     * Overi jestli se parametr nazevVeci rovná s názvem veci co chce
     * @param nazevVeci
     */
    public Boolean overJestliChceTuhleVec(String nazevVeci){
        return this.nazevVeciCoChce.equals(nazevVeci);
    }

    //== Soukromé metody (instancí i třídy) ========================================

}
