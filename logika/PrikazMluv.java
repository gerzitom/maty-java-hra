/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package logika;

import java.util.Set;
import java.util.HashSet;


/*******************************************************************************
 * Instance třídy PrikazMluv představují ...
 *
 * @author    Matyáš Frank
 * @version   0.00.000
 */
public class PrikazMluv implements IPrikaz
{
    //== Datové atributy (statické i instancí)======================================
    
    private static final String NAZEV = "mluv";
    private HerniPlan plan;

    //== Konstruktory a tovární metody =============================================

    /***************************************************************************
     *  Konstruktor ....
     */
    public PrikazMluv(HerniPlan plan){
        this.plan = plan;
    }

    //== Nesoukromé metody (instancí i třídy) ======================================

    public String provedPrikaz(String... parametry){
       if (parametry.length == 0) {
           // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Musíš mi něco říct";
       }
       if (parametry.length == 1){ //ověřuje, že se zadal jen jeden parametr a provadi prikaz Mluv, definuje seznam postav z get sezhnam postav
           String jmenoPostavy = parametry[0];
           Set<Postava> seznamPostav = this.plan.getSeznamPostav();
           
           for(Postava postava : seznamPostav){ //Prochází všechny postavy a hledá tu, která se rovná názvu parametru
               if(postava.getJmeno().equals(jmenoPostavy)){

                   // pokud je batoh plný, tak se může rozhovor provést vícekrát a postava dá hráči už jen lano
                   if (this.plan.getBatoh().najdiVBatohu("lano") == null && postava.jeSpokojena()){
                       System.out.println("Aaa, vidím, že ještě nemáš lano, tak tady si ho vem");
                       this.plan.getBatoh().vlozVec(new Klic("lano", "strom"));
                   } else {
                       postava.zacinaBratVeci();
                       System.out.println(postava.getRec());
                   }
               }
           }
           
           return null;
       }
       else {
            return "Řekl jsi " + parametry[0];
       }
    }
    //pracuje se seznamem prikazu IPrikaz
    public String getNazev(){
        return NAZEV;
    }

    //== Soukromé metody (instancí i třídy) ========================================

}
